package com.beone.admin.utils.result;

/**
 * @Title 返回错误信息
 * @Author 庞绪 on 2018/3/30.
 * @Copyright ©长沙科权
 */
public enum ResultCode {
    /*
    错误编码:
1    执行成功
0    执行失败
-1   登录密码错
100  信息已认证!repetition
101  已报价offer
     */
    REPETITION_OFFER_PRICE("101","重复报价"),//重复报价
    USER_AUTH_REPETITION("100","个人信息已认证"),//个人信息已认证!
    //登录密码错
    LOGIN_PASSWORD_ERROR("-1","登录密码错"),
    //请求成功
    SUCCESS("1","请求成功"),
    //请求失败
    FAIL("0","请求失败"),
    PHONE_FAIL("2","当前手机号码未注册!请重新输入"),
    USER_LOCK("3","当前用户状态为已锁定!请与客服联系"),
    USER_FORBID("4","当前用户状态为禁止登录!请与客服联系"),
    //必填参数为空
    PARAMETER_IS_NULL("21001","必填参数为空"),
    //文件不存在
    NOT_EXIST_FILE("21010","文件不存在"),
    //银行卡认证失败
    BANK_AUTH_FAIL("3000","银行卡认证失败"),
    //个人信息未审核
    USER_AUTH_FAIL("3001","个人信息未审核"),
    //车辆信息未审核
    CAR_AUTH_FAIL("3002","车辆信息未审核"),
    //服务已开通
    USER_SERVICE_OPEN("3003","服务已开通"),
    ORDER_STATUS_WAIT("103","当前车主有支付中或未完成的订单"),
    REPETITION_GOODS_ERROR("104","重复发货"),//repetition
    ORDER_PRICE_ERROR("105","同一车主同一时间区间允许5单"),
    ORDER_PRICE_ERROR2("106","同一车主允许10单"),
    ORDER_PRICE_ERROR3("107","同一车主同一时间区间只能有一个运输单"),
    //ExtractCode
    EXTRACT_CODE_ERROR("102","提款码有误");
    private String key;

    private String value;

    ResultCode(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
