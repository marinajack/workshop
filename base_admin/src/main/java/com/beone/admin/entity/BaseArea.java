package com.beone.admin.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title  实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
@TableName("base_area")
public class BaseArea extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 区域编号
     */
    @TableId("area_id")
	private Integer areaId;
    /**
     * 区域名称
     */
	@TableField("area_name")
	private String areaName;
    /**
     * 父级编号
     */
	@TableField("parent_id")
	private Integer parentId;
    /**
     * 区域等级(1省/2市/3区县)
     */
	@TableField("area_level")
	private Integer areaLevel;
    /**
     * 状态（1可用/0不可用）
     */
	private Integer status;


	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getAreaLevel() {
		return areaLevel;
	}

	public void setAreaLevel(Integer areaLevel) {
		this.areaLevel = areaLevel;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "BaseArea{" +
			", areaId=" + areaId +
			", areaName=" + areaName +
			", parentId=" + parentId +
			", areaLevel=" + areaLevel +
			", status=" + status +
			"}";
	}
}
