package com.beone.admin.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title 运维数据_角色信息 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@TableName("base_role")
public class BaseRole extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
	@TableId(value="role_id", type= IdType.AUTO)
	private Integer roleId;
    /**
     * 角色名称
     */
	@TableField("role_name")
	private String roleName;
    /**
     * 角色描述
     */
	@TableField("role_desc")
	private String roleDesc;
    /**
     *  是否启用  0-不可用  1-可用
     */
	@TableField("is_use")
	private Integer isUse;

	@TableField(exist = false) /** 非数据库映射字段 */
	private String permissions; //权限 nodeId 集合

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}

	@Override
	public String toString() {
		return "BaseRole{" +
			", roleId=" + roleId +
			", roleName=" + roleName +
			", roleDesc=" + roleDesc +
			", isUse=" + isUse +
			"}";
	}
}
