package com.beone.admin.service.impl;

import com.beone.admin.entity.BaseArea;
import com.beone.admin.mapper.BaseAreaMapper;
import com.beone.admin.service.BaseAreaService;
import com.base.SuperServiceImpl;
import org.springframework.stereotype.Service;


/**
 * @Title  服务实现类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
@Service
public class BaseAreaServiceImpl extends SuperServiceImpl<BaseAreaMapper, BaseArea> implements BaseAreaService {

}
