package com.base.common.datasource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @title    该注解注释在service方法上，标注为链接slaves库
 * @Author 覃球球
 * @Version 1.0 on 2017/12/6.
 * @Copyright 长笛龙吟
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadOnlyConnection {
}
